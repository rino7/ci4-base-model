<?php
namespace Dolmedo\BaseModel;

use CodeIgniter\Model;

class BaseModel extends Model
{

    protected $_relations = [];
    protected $_hasCustomSelectFields = false;

    public function __construct()
    {
        parent::__construct();
    }

    public function select($select = '*', $escape = null)
    {
        $this->_hasCustomSelectFields = true;
        $customFields = explode(",", $select);
        $fields = [];
        $tableName = $this->table;
        //Le agrego el alias de la tabla. Ej: "usuario."
        $addAliasToField = function ($item) use ($tableName) {
            if (mb_stristr($item, "{$tableName}.") === false) {
                return "{$tableName}." . trim($item);
            }
        };
        $fields = array_map($addAliasToField, $customFields);
        $select = implode(",", $fields);
        return parent::select($select, $escape);
    }

    public function first()
    {
        if (!$this->_hasCustomSelectFields) {
            $this->builder()->select($this->table . ".*");
        }
        return parent::first();
    }

    public function find($id = null)
    {
        if (!$this->_hasCustomSelectFields) {
            $this->builder()->select($this->table . ".*");
        }
        return parent::find($id);
    }

    public function findAll(int $limit = null, int $offset = null)
    {
        $limit = $limit === null ? 0 : $limit;
        $offset = $offset === null ? 0 : $offset;

        if (!$this->_hasCustomSelectFields) {
            $this->builder()->select($this->table . ".*");
        }
        return parent::findAll($limit, $offset);
    }

    public function with($relationship, $fields = null, $joinType = "LEFT")
    {

        $builder = $this->builder();

        if (isset($this->_relations[$relationship])) {
            $relation_data = $this->_relations[$relationship];
            if (is_string($relation_data)) { //uso la config por default
                $rel_table = $relation_data;
                $options = array(
                    'child_table' => $rel_table,
                    'fk_parent_table' => 'id_' . $rel_table,
                    'pk_child_table' => 'id_' . $rel_table,
                );
            } elseif (is_array($relation_data)) {
                $options = $relation_data;
            }
            $child_table = $options['child_table'];

            if (!is_array($fields)) {
                $fields = $this->db->getFieldNames($child_table);
            }
            $prefixed_array = array();

            foreach ($fields as $field => $alias) {
                //El $alias puede ser efectivamente el alias o puede ser el nombre del campo
                if (is_string($field)) {
                    //Tiene 'alias' personalizado. Ej: id_paciente_nombre => 'nombre_paciente'
                    $prefixed_array[] = "{$relationship}.{$field} as {$alias}";
                } else {
                    //No tiene un alias predefinido, le pongo yo el alias
                    $prefixed_array[] = "{$relationship}.{$alias} as {$options['fk_parent_table']}_{$alias}";
                }
            }
            $rel_fields = implode(",", $prefixed_array);
            $builder->select($rel_fields);

            $parent_table = isset($options["parent_table"]) ? $options["parent_table"] : $this->table;
            $builder->join("{$child_table} as {$relationship}", $relationship . "." . $options['pk_child_table'] . " = " . $parent_table . "." . $options['fk_parent_table'], $joinType);
        }
        return $this;
    }
    public function addFilters(array $aFilterNames)
    {
        foreach ($aFilterNames as $filterName) {
            $upperFirstFilterName = \ucfirst($filterName);
            $methodName = "filterBy{$upperFirstFilterName}";
            if (method_exists($this, $methodName)) {
                $this->$methodName();
            } else {
                throw new \Exception("No está definido el filtro {$methodName}");
            }
        }
        return $this;
    }
}
